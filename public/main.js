var socket = io.connect('http://localhost:3000', { 'forceNew': true });

socket.on('messages', function(data){
	console.log(data);
	render(data);
});

function render(data) {
	var html = data.map(function(elemento, index) {
		return (`<div>
					<strong>${elemento.author} : </strong>
					<em>${elemento.text}</em>
				</div>`)
	}).join(" ");

	document.getElementById('messages').innerHTML = html;
}

function addMessage(e){
	var cargar = {
		author: document.getElementById('author').value,
		text: document.getElementById('texto').value
	};

	socket.emit('new-message', cargar);
	return false
}